package fr.uvsq.tod;

public class Tache {
	private final long id;
	private final String title;
	
	public Tache(long id, String title){
		this.id = id;
		this.title = title;
	}
	
	public long getId(){
		return id;
	}
	
	public String getTitle(){
		return title;
	}
}
