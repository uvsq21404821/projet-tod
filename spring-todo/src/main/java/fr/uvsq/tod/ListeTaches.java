package fr.uvsq.tod;

import java.util.ArrayList;

public class ListeTaches {
	private ArrayList<Tache> l;
	
	public ListeTaches(){
		this.l = new ArrayList<Tache>();
	}
	
	public void add(Tache t){
		l.add(t);
	}
	
	public ArrayList<Tache> getListeTaches(){
		return this.l;
	}

	
	
}
