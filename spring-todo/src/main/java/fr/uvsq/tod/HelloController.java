package fr.uvsq.tod;

import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
public class HelloController {

	private final AtomicLong counter = new AtomicLong();	
	ListeTaches taches = new ListeTaches();
	
	public HelloController(){
		taches.add(new Tache(counter.incrementAndGet(), "tache1"));
		taches.add(new Tache(counter.incrementAndGet(), "tache2"));
		taches.add(new Tache(counter.incrementAndGet(), "tache3"));
	}
//    @RequestMapping("/")
//    public String HTTP_GET() {
//        return "Simple message";
//    }
    
//    @RequestMapping("/")
//    public Tache getTaches(@RequestParam(value="name", defaultValue="World") String name){
//    	return new Tache(counter.incrementAndGet(), String.format(template, name));
//    }
	
	@RequestMapping(method = RequestMethod.GET)
	public ArrayList<Tache> getTaches(/*@RequestParam(value="name", defaultValue="World") String name*/){
		return taches.getListeTaches();
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public void ajout(@RequestBody String newTache){
		String nomTache = newTache.substring(newTache.indexOf("=")+1, newTache.length());
		taches.add(new Tache(counter.incrementAndGet(), nomTache));
	}

}